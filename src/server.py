#!/usr/bin/python
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree.

import logging
from pathlib import Path
import shutil
import tempfile
import urllib
from flask import Flask, jsonify, request, Response, send_file, abort, render_template, url_for
from flask.logging import default_handler
from flask_restx import Api, Resource, reqparse, apidoc
from werkzeug.datastructures import FileStorage
from flask_jwt_extended import verify_jwt_in_request


from qwc_services_core.api import CaseInsensitiveArgument
from qwc_services_core.auth import (
    auth_manager,
    get_identity,
    get_username,
)
from qwc_services_core.runtime_config import RuntimeConfig
from qwc_services_core.tenant_handler import TenantHandler

from access_control import AccessControl


# API
class MultiTenantApi(Api):
    # handles dynamic URL prefixed by tenant
    @property
    def base_path(self):
        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        multi_tenant = config.get("multi_tenant_api", False)
        if multi_tenant:
            base_path = f"/{tenant}/" + url_for(self.endpoint("root")).lstrip('/')
            return base_path
        else:
            return url_for(self.endpoint("root"))


@apidoc.apidoc.add_app_template_global
def swagger_static(filename):
    return "{0}/swaggerui/{1}".format(api.base_path.rstrip('/'), filename)


app = Flask(__name__)
api = MultiTenantApi(app, version='1.0', title='Publish API',
                      description='API for QWC Publish service',
                      default_label='Publish operations', doc='/api/')

project_ns = api.namespace('project', description='projects operations')
folder_ns = api.namespace('folder', description='folders operations')

app.url_map.strict_slashes = False

# request parser
projectList_post_parser = reqparse.RequestParser(argument_class=CaseInsensitiveArgument)
projectList_post_parser.add_argument('filename', type=str)
projectList_post_parser.add_argument('file', location='files', type=FileStorage, required=True)

project_get_parser = reqparse.RequestParser(argument_class=CaseInsensitiveArgument)
project_get_parser.add_argument('download', default="false", type=str)

project_put_parser = reqparse.RequestParser(argument_class=CaseInsensitiveArgument)
project_put_parser.add_argument('file', location='files', type=FileStorage, required=True)

folderList_post_parser = reqparse.RequestParser(argument_class=CaseInsensitiveArgument)
folderList_post_parser.add_argument('name', required=True, type=str)

move_parser = reqparse.RequestParser(argument_class=CaseInsensitiveArgument)
move_parser.add_argument('origin', required=True, type=str)
move_parser.add_argument('destination', required=True, type=str)

auth = auth_manager(app, api)

# configs
tenant_handler = TenantHandler(app.logger)
config_handler = RuntimeConfig("publish", app.logger)


# formatter
class RequestFormatter(logging.Formatter):
    def format(self, record):
        record.tenant = tenant_handler.tenant()
        try:
            identity = get_identity()
            record.user = get_username(identity)
        except Exception:
            record.user = 'unknown user'
        return super().format(record)


formatter = RequestFormatter(
    '[%(asctime)s] %(levelname)s in %(module)s:  %(user)s %(message)s in tenant %(tenant)s'
)
default_handler.setFormatter(formatter)


@api.documentation
def custom_ui():
    return render_template(
        "swagger-ui.html",
        title=api.title,
        specs_url="{}/swagger.json".format(api.base_path.rstrip('/'))
    )


def optional_jwt():
    try:
        if verify_jwt_in_request():
            return True
    except BaseException:
        return False


def update_config(config_generator_url, tenant):
    """Send request to QWC Config Service to update configurations"""

    req = urllib.request.Request(
        urllib.parse.urljoin(
            config_generator_url,
            "generate_configs?tenant=" + tenant
        ),
        method="POST"
    )

    r = urllib.request.urlopen(req)
    content = r.read().decode()

    if 'CRITICAL' in content:
        msg = "Unable to generate service configurations"
        app.logger.error(msg)
        return False
    else:
        msg = "Service configurations generated"
        app.logger.info(msg)
        return True


@app.before_request
def assert_publish_role():
    tenant = tenant_handler.tenant()
    config = config_handler.tenant_config(tenant)
    auth_required = config.get("auth_required", True)
    app.logger.debug(f"auth_required : {auth_required}")
    if auth_required:
        if optional_jwt():
            access_control = AccessControl(config, app.logger)
            identity = get_identity()
            if not access_control.is_publisher(identity):
                msg = "Access denied"
                app.logger.debug(msg)
                api.abort(401, msg)
            app.logger.debug("access granted")
        else:
            msg = "Authentication required"
            app.logger.debug(msg)
            api.abort(401, msg)


@project_ns.route('/')
class ProjectList(Resource):
    def get(self):
        '''List all QGIS projects, with their relative path, in QWC Scan path of current tenant'''
        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))
        project_type = config.get("qgis_project_type", "qgs").lstrip(".")
        result = []

        if qgis_projects_scan_base_dir.is_dir():
            result = [
                file.relative_to(qgis_projects_scan_base_dir).as_posix()
                for file in qgis_projects_scan_base_dir.rglob('*.*')
                if file.suffix.lstrip(".") == project_type
            ]
        else:
            return app.logger.error("qgis_projects_scan_base_dir not defined")

        app.logger.info('list projects')
        return jsonify({"projects": result})

    @project_ns.expect(projectList_post_parser)
    @api.param('filename', 'Relative project path in qgis_projects_scan_base_dir folder')
    @api.param('file', 'QGIS Project data (with .qgs extension)')
    def post(self):
        '''Create a new QGIS project in QWC Scan path of current tenant'''
        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        project_type = config.get("qgis_project_type", "qgs").lstrip(".")
        # check if the post request has the file part
        if 'file' not in request.files:
            app.logger.debug('No file part')
            api.abort(404, "No file part")
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            app.logger.debug('No selected file')
            api.abort(404, "No selected file")
        if '.' in file.filename and file.filename.split('.')[-1].lower() != project_type:
            app.logger.debug(f"File not allowed")
            api.abort(404, "File not allowed")

        data = projectList_post_parser.parse_args()
        if "filename" in data and data["filename"]:
            project_name = data["filename"]
        else:
            project_name = file.filename

        update_config_enable = config.get("update_config_enable")
        config_generator_service_url = config.get("config_generator_service_url")
        qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))

        project_path = qgis_projects_scan_base_dir / project_name

        if project_path.exists():
            app.logger.info(f"project '{project_name}' already exist")
            abort(403, "Project already exist")

        try:
            file.save(project_path)
        except Exception as e:
            msg = f"Unable to create project {project_name}"
            app.logger.error(msg)
            app.logger.debug(f"Error : {str(e)}")
            abort(500, msg)

        if update_config_enable:
            try:
                conf_updated = update_config(config_generator_service_url, tenant)
            except Exception as e:
                conf_updated = False
                app.logger.debug('Error : "%s"' % str(e))
            if not conf_updated:
                # Rollback
                project_path.unlink()
                abort(500, "Unable to generate service configurations")

        app.logger.info(f"create project : '{project_name}'")
        return jsonify({'success': f"Project {project_name} created"})


@project_ns.route('/<path:filename>')
class Project(Resource):
    @project_ns.expect(project_get_parser)
    @api.param('download', 'If `true`, response will be a file')
    def get(self, filename):
        '''Get content of specific QGIS project, in QWC Scan path of current tenant'''
        data = project_get_parser.parse_args()
        download = data['download'] in ["true", "1"]

        if not filename:
            abort(422, "filename parameter is required")

        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))

        project_path = qgis_projects_scan_base_dir / filename
        if project_path.is_file():
            if download:
                app.logger.info(f"download project '{filename}'")
                return send_file(project_path, as_attachment=True)
            else:
                result = project_path.read_text(encoding="utf-8")
                app.logger.info(f"get project : '{filename}'")
                return Response(result, mimetype='text/xml')
        else:
            msg = f"Project file '{filename}' does not exist"
            app.logger.info(msg)
            abort(404, msg)

    @project_ns.expect(project_put_parser)
    @api.param('file', 'QGIS Project data (with .qgs extension)')
    def put(self, filename):
        '''Update a QGIS project in QWC Scan path of current tenant'''
        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        project_type = config.get("qgis_project_type", "qgs").lstrip(".")
        # check if the post request has the file part
        if 'file' not in request.files:
            api.abort(404, "No file part")
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            api.abort(404, "No selected file")
        if '.' in file.filename and file.filename.split('.')[-1].lower() != project_type:
            api.abort(404, "File not allowed")

        update_config_enable = config.get("update_config_enable")
        config_generator_service_url = config.get("config_generator_service_url")
        qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))

        project_path = qgis_projects_scan_base_dir / filename

        if not project_path.exists():
            msg = f"Project file '{filename}' does not exist"
            app.logger.info(msg)
            abort(404, msg)

        with tempfile.TemporaryDirectory() as tmpdirname:
            tmp_project_path = Path(tmpdirname) / filename
            try:
                tmp_project_path.parent.mkdir(exist_ok=True, parents=True)
                shutil.move(project_path, tmp_project_path)
                file.save(project_path)
            except Exception as e:
                msg = f"Unable to update project {filename}"
                app.logger.error(msg)
                app.logger.debug(f"Error : {str(e)}")
                abort(500, msg)

            if update_config_enable:
                try:
                    conf_updated = update_config(config_generator_service_url, tenant)
                except Exception as e:
                    conf_updated = False
                    app.logger.debug('Error : "%s"' % str(e))
                if not conf_updated:
                    # Rollback
                    project_path.unlink()
                    shutil.move(tmp_project_path, project_path)
                    abort(500, "Unable to generate service configurations")

        app.logger.info(f"update project '{filename}'")
        return jsonify({'success': f"Project {filename} updated"})

    def delete(self, filename):
        '''Delete specific QGIS project, in QWC Scan path of current tenant'''

        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))
        update_config_enable = config.get("update_config_enable")
        config_generator_service_url = config.get("config_generator_service_url")

        project_path = qgis_projects_scan_base_dir / filename
        if project_path.is_file():
            # tmpdir use for rollback
            with tempfile.TemporaryDirectory() as tmpdirname:
                tmp_project_path = Path(tmpdirname) / filename
                try:
                    tmp_project_path.parent.mkdir(exist_ok=True, parents=True)
                    shutil.move(project_path, tmp_project_path)
                except Exception as e:
                    msg = f"Unable to delete file '{filename}'"
                    app.logger.error(msg)
                    app.logger.debug(f"Error : {str(e)}")
                    abort(500, msg)

                if update_config_enable:
                    try:
                        conf_updated = update_config(config_generator_service_url, tenant)
                    except Exception as e:
                        conf_updated = False
                        app.logger.debug('Error : "%s"' % str(e))
                    if not conf_updated:
                        # Rollback
                        shutil.move(tmp_project_path, project_path)
                        abort(500, "Unable to generate service configurations")

                app.logger.info(f"delete project '{filename}'")
                return jsonify({'success': f"Project {filename} deleted"})

        else:
            msg = f"Project file {filename} empty or does not exist"
            app.logger.error(msg)
            abort(404, msg)


@folder_ns.route('/')
class FolderList(Resource):
    def get(self):
        '''List all folders, with their relative path, in QWC Scan path of current tenant'''
        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))
        result = []

        if qgis_projects_scan_base_dir.is_dir():
            result = [
                folder.relative_to(qgis_projects_scan_base_dir).as_posix()
                for folder in qgis_projects_scan_base_dir.rglob('*')
                if folder.is_dir()
            ]
        else:
            return app.logger.error("qgis_projects_scan_base_dir not defined")

        app.logger.info("list folders")
        return jsonify({"folders": result})

    @folder_ns.expect(folderList_post_parser)
    @api.param('name', 'Relative folder path in qgis_projects_scan_base_dir folder')
    def post(self):
        '''Create a new folder in QWC Scan path of current tenant'''
        data = folderList_post_parser.parse_args()
        if "name" not in data:
            abort(422, "No name for the new folder")

        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))

        new_folder_name = data["name"]
        new_folder_path = qgis_projects_scan_base_dir / new_folder_name
        try:
            new_folder_path.mkdir(parents=True)
        except Exception as e:
            msg = f"Unable to create folder {new_folder_name}"
            app.logger.error(msg)
            app.logger.debug(f"Error : {str(e)}")
            abort(500, msg)

        app.logger.info(f"create folder '{new_folder_name}'")
        return jsonify({'success': f"Folder {new_folder_name} created"})


@folder_ns.route('/<path:dirname>')
class Folder(Resource):
    def delete(self, dirname):
        '''Delete specific folder and his content, in QWC Scan path of current tenant'''

        tenant = tenant_handler.tenant()
        config = config_handler.tenant_config(tenant)
        qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))
        update_config_enable = config.get("update_config_enable")
        config_generator_service_url = config.get("config_generator_service_url")

        folder_path = qgis_projects_scan_base_dir / dirname
        if folder_path.is_dir():
            # tmpdir use for rollback
            with tempfile.TemporaryDirectory() as tmpdirname:
                tmp_folder_path = Path(tmpdirname) / dirname
                try:
                    tmp_folder_path.mkdir(exist_ok=True, parents=True)
                    shutil.move(folder_path, tmp_folder_path)
                except Exception as e:
                    msg = f"Unable to delete folder {dirname}"
                    app.logger.error(msg)
                    app.logger.debug(f"Error : {str(e)}")
                    abort(500, msg)

                if update_config_enable:
                    try:
                        conf_updated = update_config(config_generator_service_url, tenant)
                    except Exception as e:
                        conf_updated = False
                        app.logger.debug('Error : "%s"' % str(e))
                    if not conf_updated:
                        # Rollback
                        shutil.move(tmp_folder_path, folder_path)
                        abort(500, "Unable to generate service configurations")

                app.logger.info(f"delete folder '{dirname}'")
                return jsonify({'success': f"Folder {dirname} deleted"})
        else:
            msg = f"Folder {dirname} does not exist"
            app.logger.error(msg)
            abort(404, msg)


def scan_dir_to_json(directory):
    '''Scan directory recursively and export it in json'''
    result = {
        "name": directory.name,
        "projects": [],
        "folders": []
    }
    tenant = tenant_handler.tenant()
    config = config_handler.tenant_config(tenant)
    project_type = config.get("qgis_project_type", "qgs").lstrip(".")
    for obj in directory.glob('*'):
        if obj.is_file() and obj.suffix.lstrip(".") == project_type:
            result["projects"].append(obj.name)
        if obj.is_dir():
            result["folders"].append(scan_dir_to_json(obj))
    return result


@app.route('/move', methods=['POST'])
@api.expect(move_parser)
@api.param('origin', 'Origin of project or folder path in qgis_projects_scan_base_dir folder')
@api.param('destination', 'Destination of project or folder path in qgis_projects_scan_base_dir folder')
def move():
    '''Move QGIS project or folder to another location (relative path), in QWC Scan path of current tenant'''
    tenant = tenant_handler.tenant()
    config = config_handler.tenant_config(tenant)
    qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))
    update_config_enable = config.get("update_config_enable")
    config_generator_service_url = config.get("config_generator_service_url")

    data = move_parser.parse_args()
    origin_filename = data["origin"].lstrip("/")
    destination_filename = data["destination"].lstrip("/")

    origin_path = qgis_projects_scan_base_dir / origin_filename
    if origin_path.is_file() or origin_path.is_dir():
        destination_path = qgis_projects_scan_base_dir / destination_filename
        if not destination_path.exists():
            try:
                destination_path.parent.mkdir(exist_ok=True, parents=True)
                shutil.move(origin_path, destination_path)
            except Exception as e:
                msg = f"Unable to move file {origin_filename} to {destination_filename}"
                app.logger.error(msg)
                app.logger.debug(f"Error : {str(e)}")
                abort(500, msg)

            if update_config_enable:
                try:
                    conf_updated = update_config(config_generator_service_url, tenant)
                except Exception as e:
                    conf_updated = False
                    app.logger.debug('Error : "%s"' % str(e))
                if not conf_updated:
                    # Rollback
                    shutil.move(destination_path, origin_path)
                    abort(500, "Unable to generate service configurations")

            app.logger.info(f"move project or folder '{origin_filename}' to '{destination_filename}'")
            return jsonify({'success': f"Project or folder {origin_filename} moved to {destination_filename}"})
        else:
            msg = f"Project file or folder destination {destination_filename} already exist"
            app.logger.error(msg)
            abort(404, msg)
    else:
        msg = f"Project file  or folder {origin_filename} does not exist"
        app.logger.error(msg)
        abort(404, msg)


@app.route('/listprojectdir', methods=['GET'])
def list_projects():
    '''Generate json tree architecture of folders and projects in the QWC Scan path of current tenant'''
    tenant = tenant_handler.tenant()
    config = config_handler.tenant_config(tenant)
    project_type = config.get("qgis_project_type", "qgs").lstrip(".")
    qgis_projects_scan_base_dir = Path(config.get("qgis_projects_scan_base_dir"))
    result = {
        "name": "/",
        "projects": [],
        "folders": []
    }

    if qgis_projects_scan_base_dir.is_dir():
        for obj in qgis_projects_scan_base_dir.glob('*'):
            if obj.is_file() and obj.suffix.lstrip(".") == project_type:
                result["projects"].append(obj.name)
            if obj.is_dir():
                result["folders"].append(scan_dir_to_json(obj))
    else:
        return app.logger.error("qgis_projects_scan_base_dir not defined")

    app.logger.info("list projects and folders")
    return jsonify(result)


@app.route('/getprojecttype', methods=['GET'])
def get_project_type():
    '''Return the project type define in configuration'''
    tenant = tenant_handler.tenant()
    config = config_handler.tenant_config(tenant)
    project_type = config.get("qgis_project_type", "qgs")
    return jsonify({"type": project_type})


@app.route("/ready", methods=['GET'])
def ready():
    """ readyness probe endpoint """
    return jsonify({"status": "OK"})


@app.route("/healthz", methods=['GET'])
def healthz():
    """ liveness probe endpoint """
    return jsonify({"status": "OK"})


if __name__ == "__main__":
    print("Starting publish service...")
    app.logger.setLevel(logging.DEBUG)
    app.run(host='localhost', port=5100, debug=True)
