from qwc_services_core.config_models import ConfigModels
from qwc_services_core.database import DatabaseEngine
from qwc_services_core.auth import (
    get_username,
    get_groups,
)


class AccessControl:

    def __init__(self, config, logger):
        """Constructor

        :param str tenant: Tenant ID
        :param Logger logger: Application logger
        """
        self.logger = logger

        self.config = config

        db_engine = DatabaseEngine()
        conn_str = self.config.get('config_db_url', 'postgresql:///?service=qwc_configdb')
        self.config_models = ConfigModels(db_engine, conn_str)

    def is_publisher(self, identity):
        """Check if user is in publishers group

        :param str identity: User identity
        """
        username = get_username(identity)
        publisher_users_names = self.config.get('publisher_users_names', None)
        if publisher_users_names and username in publisher_users_names:
            self.logger.debug("is defined in publisher_users_names")
            return True

        groups = get_groups(identity)
        # With the qwc-db-auth groups aren't store in the identity so we need to get them from db
        if len(groups) == 0:
            groups = self.get_user_groups(username)

        publisher_groups_names = self.config.get('publisher_groups_names', None)
        group_search_operator = self.config.get('group_search_operator', None)
        if publisher_groups_names:
            if group_search_operator == 'OR':
                if (set(publisher_groups_names) & set(groups)):
                    self.logger.debug("is in a group of publisher_groups_names")
                    return True
            elif group_search_operator == 'AND':
                if set(publisher_groups_names).issubset(set(groups)):
                    self.logger.debug("is in all group of publisher_groups_names")
                    return True

        if self.has_publishing_role(username, groups):
            self.logger.debug("has the publisher_role_name")
            return True

        return False

    def get_user_groups(self, username):
        session = self.config_models.session()

        Group = self.config_models.model('groups')
        User = self.config_models.model('users')

        # create query
        result = session.query(Group) \
            .join(Group.users_collection) \
            .filter(User.name == username).all()

        user_groups = [row.name for row in result]

        session.close()

        return user_groups

    def has_publishing_role(self, username, groups):
        """Create base query for all permissions of a user and group.
        Combine permissions from roles of user and user groups, groups roles and
        public role.

        :param str username: User name
        :param list or str groups: Groups names
        :param Session session: DB session
        :param str publisher_role_name: Publishers role name
        """
        session = self.config_models.session()

        Role = self.config_models.model('roles')
        Group = self.config_models.model('groups')
        User = self.config_models.model('users')

        publisher_role_name = self.config.get('publisher_role_name', None)

        # create query
        query = session.query(Role)

        # query permissions from roles in user groups
        user_groups_roles_query = query.join(Role.groups_collection) \
            .join(Group.users_collection) \
            .filter(User.name == username)

        # query permissions from direct user roles
        user_roles_query = query.join(Role.users_collection) \
            .filter(User.name == username)

        # query permissions from group roles
        group_roles_query = query.join(Role.groups_collection) \
            .filter(Group.name.in_(groups))

        # combine queries
        query = user_groups_roles_query.union(user_roles_query) \
            .union(group_roles_query) \
            .filter(Role.name == publisher_role_name)

        (has_role, ), = session.query(query.exists())

        session.close()

        return has_role
