QWC publish service
=================

This service provide an API to publish and manage the projects in a QWC2 instance. The service allows you to organize, add, update and delete QGIS project files in a QWC2 instance. 

This initial work has been funded by Les Agences de l eau | Direction des Systèmes d’Information et des Usages Numériques (https://www.lesagencesdeleau.fr/)

API documentation:

    http://localhost:5100/api/

This service can be used with a QGIS plugin [QWC2 Tools](https://gitlab.com/Oslandia/qgis/qwc2_tools) to easily publish project directly from QGIS Desktop.


Configuration
-------------

The static config files are stored as JSON files in `$CONFIG_PATH` with subdirectories for each tenant,
e.g. `$CONFIG_PATH/default/*.json`. The default tenant name is `default`.

### JSON config

* [JSON schema](schemas/qwc-publish-service.json)
* File location: `$CONFIG_PATH/<tenant>/publishConfig.json`

Example:
```json
{
  "$schema": "https://gitlab.com/Oslandia/qwc/qwc-publish-service/-/raw/main/schemas/qwc-publish-service.json",
  "service": "publish",
  "config": {
    "qgis_projects_scan_base_dir": "/data/demo/scan",
    "publisher_users_names": ["admin"],
    "publisher_groups_names":["publisher","admin"],
    "group_search_operator": "OR",
    "publisher_role_name": "publisher",
    "update_config_enable": true,
    "auth_required": false,
    "multi_tenant_api": true,
    "qgis_project_type": "qgs",
    "config_generator_service_url": "http://qwc-config-service:9090"
  }
}
```
`qgis_projects_scan_base_dir` is the base directory where qgis project are scanned by qwc-config-generator

To define access to this service, several options are available : 

- Define a list of the users names allowed to publish in `publisher_users_names`

- Define a list of the groups names allowed to publish `publisher_groups_names` combine with a `group_search_operator`. If `group_search_operator` is set to 'AND', user must to be part of all groups, else if is set to 'OR', user must be part of at least one group.

- Define a role required to publish a project in `publisher_role_name`.

all options above can be used in parallel and will be applied in the following order : is user in `publisher_users_names`, does the user have one or all `publisher_groups_names` and does the user have the role `publisher_role_name`.
 
`update_config_enable` allow qwc_project_publisher_service to run config_generator_service (at url `config_generator_service_url`) after a user publish or delete project.

`auth_required` if True you need to be authenticated to use this service

`multi_tenant_api` need to be set to true in multi-tenant to have swagger api working for each tenant

`qgis_project_type` is the QGIS project type used for publication


Usage
-----

List all projects :

`curl -v -X GET "http://127.0.0.1:5100/project/"`

Publish a new project :

`curl -v -X POST "http://127.0.0.1:5100/project/" --form 'file=@"/path/to/my/project.qgs"' --form 'filename="new_project.qgs"'`

Get a project (get .qgs project content (xml)) :

`curl -v -X GET "http://127.0.0.1:5100/project/new_project.qgs"`

Get a project (download .qgs project) :

`curl -v -X GET "http://127.0.0.1:5100/project/new_project.qgs?download=true"`

Update a project :

`curl -v -X PUT "http://127.0.0.1:5100/project/new_project.qgs" --form 'file=@"/path/to/my/project.qgs"'`

Delete a project :

`curl -v -X DELETE "http://127.0.0.1:5100/project/new_project.qgs"`

List all folders :

`curl -v -X GET "http://127.0.0.1:5100/folder/"`

Add a folder :

`curl -v -X POST "http://127.0.0.1:5100/folder/" --form 'name="my_folder"'`

Delete a folder :

`curl -v -X DELETE "http://127.0.0.1:5100/folder/my_folder"`

Move project or folder :

`curl -v -X POST "http://127.0.0.1:5100/move" --form 'origin="/my_project.qgs"' --form 'destination="/new/path/to/my_project.qgs"'`

Get file and folder tree in json format :

`curl -v -X GET "http://127.0.0.1:5100/listprojectdir"`

Get project type (qgs or qgz) define in configuration : 

`curl -v -X GET "http://127.0.0.1:5100/getprojecttype"`

N.B. : If `auth_required` = `True`, X-CSRF-TOKEN header and cookies are required.</br>
Add `-H "X-CSRF-TOKEN: xxxxxxxx" -b cookiefilepath` to cURL command.<br>
Use cURL POST command to login in.<br>
Look at `POST_PARAM_LOGIN` in https://github.com/qwc-services/qwc-db-auth

Development
-----------

Create a virtual environment:

    virtualenv --python=/usr/bin/python3 .venv

Activate virtual environment:

    source .venv/bin/activate

Install requirements:

    pip install -r requirements.txt

Set the `CONFIG_PATH` environment variable to the path containing the service config and permission files when starting this service (default: `config`).

    export CONFIG_PATH=../qwc-docker/volumes/config

Configure environment:

    echo FLASK_ENV=development >.flaskenv

Start local service:

    python server.py 
